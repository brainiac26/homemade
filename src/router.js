import Vue from 'vue'
import Router from 'vue-router'

import Legal from './views/Legal'
import Landing from './views/Landing'
import Contact from './views/Contact'
import Faq from './views/FAQ'
import Team from './views/Team'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'landing',
      component: Landing
    },
    {
      path: '/legal',
      name: 'legal',
      component: Legal
    },
    {
      path: '/contact',
      name: 'contact',
      component: Contact
    },
    {
      path: '/faq',
      name: 'contact',
      component: Faq
    },
    {
      path: '/team',
      name: 'contact',
      component: Team
    },
  ]
})
