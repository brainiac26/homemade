import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'

import App from './App.vue'
import router from './router'
import store from './store'
import './registerServiceWorker'

import 'mdbvue/build/css/mdb.css';
import './styles/bootstrap/bootstrap.scss';

import './styles/style.scss';
import './styles/fonts.scss';

import 'vue-slider-component/theme/antd.css'


Vue.config.productionTip = false

Vue.use(BootstrapVue)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
